# Palette theme #
============

The Palette theme is a sub-theme of SmartWeb, inheriting template files from the main theme Rooty. It can be used by theme designers wanting to change Palette.

**Demo:**

* [Demo store](http://palette-theme.smartweb.dk/)


## Documentation ##

* [Getting started - in Danish](http://design-help-new.smart-web.dk/35-template-udvikling/).
* [Theme Documentation - in Danish](http://design-help-new.smart-web.dk/3-for-udviklere/).
* Need more help? Contact our [Support](http://www.get-smartweb.com/support/).