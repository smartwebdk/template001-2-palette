{strip}
{*** Framework CSS ***}
{addLink href='assets/css/libs.css'}

{*** Template CSS ***}
{addLink href='assets/css/template.css'}

{*** Theme CSS ***}
{addLink href='assets/css/theme.css'}

{*** Print CSS ***}
{addLink href='assets/css/print.css' media='print'}

{*** Framework Javascript ***}
{$bundleVersion = $template.settings.BUNDLE_VERSION}
{if isset($bundleVersion) && $bundleVersion !== "1.0.0"}
    {addScript src="assets/js/{$bundleVersion}/app.js"}
{else}
    {addScript src='assets/js/app.js'}
{/if}

{*** TemplateJS ***}
{addScript src='assets/js/template.js'}

{collection assign=boxes controller=moduleBox}
{$boxes = $boxes->groupBy('Position')}

{* Define standard class for columns *}
{$columnClass = "col-s-4 col-m-12 col-l-12 col-xl-24"}
{assign var=columns value=0 scope=global}

{* Calculate number of columns needed *}
{if !empty($boxes.left) and !empty($boxes.right)}
    {$columnClass = "col-s-4 col-m-12 col-l-6 col-xl-14"}
    {$columns = 2}
{elseif !empty($boxes.left) or !empty($boxes.right)}
    {$columnClass = "col-s-4 col-m-12 col-l-9 col-xl-19"}
    {$columns = 1}
{/if}

{/strip}<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6 ielt9 no-js" lang="{$general.languageIso639}"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7 ielt9 no-js" lang="{$general.languageIso639}"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 ielt9 no-js" lang="{$general.languageIso639}"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9 no-js" lang="{$general.languageIso639}"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="{$general.languageIso639}"> <!--<![endif]-->
<head prefix="og: http://ogp.me/ns#
              fb: http://ogp.me/ns/fb#
              ng: http://angularjs.org">
    {head_include}
</head>
<body id="ng-app" data-ng-app="platform-app" class="site{$general.siteId} language{$general.languageIso} currency{$general.currencyIso} pageId{$page.id} pageCategory{$page.categoryId} pageType{$page.name}">

{* Language and currency selectors, search, top navigation *}
{include file='partials/top.tpl'}


{* Main navigation *}
<nav class="site-navigation trunk--slide slideRight trunk--close is-hidden-print">

    {$productLink = {page id=$page.productPageId print=Link}}

    {if $template.settings.SETTINGS_SHOW_SEARCH}
        <div class="trunk--group">
            <form method="get" action="/{if {$general.isShop} == '1'}{$productLink}{else}{$Text.SEARCH_LINK}{/if}/">
                <div class="input-group xsmall">
                    <input type="text" class="form-input input-group-main" placeholder="{$Text.SEARCH_TEXT}" name="search" required>
                    <span class="input-group-button"><button class="button" type="submit">{$text.SEARCH}</button></span>
                </div>
            </form>
        </div>
    {/if}

    <header class="trunk--group--header"><span class="h5">{$contactdata.name}</span></header>

    {*** Primary navigation ***}
    {menu assign=primaryMenu static=false}
    {include file='modules/widgets/menu/menu.tpl' items=$primaryMenu classes='list-unstyled'}

    {if $template.settings.SETTINGS_SHOW_MY_ACCOUNT && $access.user}
        <header class="trunk--group--header"><span class="h5">{$text.YOUR_ACCOUNT}</span></header>
        {menuStatic assign=userMenu}
        {include file='modules/widgets/menu/menu.tpl' items=$userMenu classes='list-unstyled'}
    {/if}

    <div class="trunk--group l-menu-dropdown-select">
        {*** Get all shop currencies ***}
        {collection controller=currency assign=currencies}

        {if $currencies and $currencies->getActualSize() gt 1}
            <div class="l-trunk-module trailing-db">
                {include file="modules/widgets/currency/currency.tpl" collection=$currencies type="dropdown"}
            </div>
        {/if}

        {*** Languages collection ***}
        {collection controller=language assign=languages}

        {if $languages->getActualSize() gt 1}
            <div class="l-trunk-module trailing-db">
                {$showFlag = true}
                {$showText = true}
                {if $template.settings.SETTINGS_TYPE_LANGUAGE eq 'FLAG'}
                    {$showText = false}
                {elseif $template.settings.SETTINGS_TYPE_LANGUAGE eq 'TEXT'}
                    {$showFlag = false}
                {/if}

                {include file="modules/widgets/language/language.tpl" collection=$languages type="dropdown" showFlag=$showFlag showText=$showText}
            </div>
        {/if}
    </div>
</nav>


{* Site content wrapper *}
<div class="site trunk--slide trunk--content slideRight">

    {* Picture element *}
    <div class="site-slider is-hidden-print">
        <div class="container site-wrapper with-xlarge">
            {include file='modules/widgets/slider/page-slider.tpl' crop=true}
        </div>
    </div>

    <div class="site-content">

        {if !$page.isFrontPage and ($template.settings.SETTINGS_SHOW_BREADCRUMB or $template.settings.SETTINGS_SHOW_PRINT)}
        <div class="container site-wrapper with-xlarge">
            <div class="row">
                <div class="col-s-3 col-m-10 col-l-10 col-xl-20">
                    {if $template.settings.SETTINGS_SHOW_BREADCRUMB}
                        {* Breadcrumbs / navigation of the entire site *}
                        {breadcrumbs}
                    {/if}
                </div>
                <div class="col-s-1 col-m-2 col-l-2 col-xl-4 text-right">
                    {if $template.settings.SETTINGS_SHOW_PRINT}
                        {* Print friendly link *}
                        <div class="m-print">
                            <a title="Printvenlig" onclick="window.print(); return false;" href="#">
                                <i class="fa fa-fw fa-print"></i>
                            </a>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
        {/if}

        <div class="container site-wrapper with-xlarge trailing-db">

            {* Notification *}
            {include file="modules/widgets/notification/notification.tpl"}

            <div class="row">
                {if !empty($boxes.left)}
                    {include file='modules/column/column.tpl' boxes=$boxes.left}
                {/if}

                <div class="{$columnClass}">
                    {* Page content and modules include *}
                    {pageTypeInclude}

                    {if $general.isShop and $page.isFrontPage}

                        {collection assign=focusProducts controller=productList focus=frontpage}

                        {if $focusProducts->getActualSize() gt 0}
                            <div class="frontpage-content">
                                <div class="row">
                                    <div class="col-s-4 col-m-12 col-l-12 col-xl-24">
                                        <header class="page-title page-title-frontpage">
                                            <span class="h1">{$text.PRODUCT_CATALOG_FOCUS_FRONTPAGE_HEADLINE}</span>
                                        </header>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-s-4 col-m-12 col-l-12 col-xl-24">
                                        {include file='modules/product/product-list-combined.tpl' productlist=$focusProducts->filter('Sorting,Title')}
                                    </div>
                                </div>
                            </div>
                        {/if}
                    {/if}
                </div>

                {if !empty($boxes.right)}
                    {include file='modules/column/column.tpl' boxes=$boxes.right}
                {/if}
            </div>
        </div>
    </div>

    {* Footer menus, contact data and payment icons *}
    {include file='partials/bottom.tpl'}
</div>

    {* General overlays *}
    {if $shop and $shop.priceTerms}
        {include file='modules/widgets/overlay/overlay.tpl' dataId='priceTerms' dataItemId=$shop.priceTerms.id}
    {/if}
    {* Cookie consent *}
    {include file='modules/widgets/cookie/cookie.tpl'}
    {body_include}
</body>
</html>
