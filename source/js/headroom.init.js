;(function ($, exports) {

	// jQuery init
	$(function() {

		// grab an element
		var header = $(".site-header")[0];
		// construct an instance of Headroom, passing the element
		var headroom  = new Headroom(header, {
			// vertical offset in px before element is first unpinned
		    offset : 100,
		    // css classes to apply
		    classes : {
		        // when element is initialised
		        initial : "headroom",
		        // when scrolling up
		        pinned : "headroom--pinned",
		        // when scrolling down
		        unpinned : "headroom--unpinned",
		        // when above offset
		        top : "headroom--top",
		        // when below offset
		        notTop : "headroom--not-top"
		    }
		});
		// initialise
		headroom.init();

	});

})(jQuery, window);