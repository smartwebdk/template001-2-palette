<footer class="site-footer">
	<div class="container with-xlarge">
		<div class="row">

			<div class="col-s-4 col-m-6 col-l-3 col-xl-6 first-col">
				<div class="footer-sitemap">
					<p class="h4">{$contactdata.name}</p>

					{menu assign=sitemapMenu static=false maxDepth=1}
					{include
						file='modules/widgets/menu/menu.tpl'
						items=$sitemapMenu
						classes='list-unstyled'
					}
				</div>
			</div>

			{if $access.user and $template.settings.SETTINGS_SHOW_MY_ACCOUNT}
				<div class="col-s-4 col-m-6 col-l-3 col-xl-6">
					<div class="footer-account">
						<p class="h4">{$text.YOUR_ACCOUNT}</p>

						{menuStatic assign=userMenu}
						{include
							file='modules/widgets/menu/menu.tpl'
							items=$userMenu
							classes='list-unstyled'
						}
					</div>
				</div>
			{/if}

			<div class="col-s-4 col-m-6 col-l-3 col-xl-6">
				<div class="footer-contact">
					<p class="h4">{$text.CONTACT_TEXT}</p>

					<ul class="list-unstyled">
						{if $contactdata.name && $template.settings.SETTINGS_SHOW_CONTACT_TITLE}
							<li><span>{$contactdata.name}</span></li>
						{/if}
						{if $contactdata.address && $template.settings.SETTINGS_SHOW_CONTACT_ADDRESS}
							<li><span>{$contactdata.address}</span></li>
						{/if}
						{if ($contactdata.zipcode && $template.settings.SETTINGS_SHOW_CONTACT_ZIPCODE) || ($contactdata.city && $template.settings.SETTINGS_SHOW_CONTACT_CITY)}
							<li><span>
								{if $contactdata.zipcode && $template.settings.SETTINGS_SHOW_CONTACT_ZIPCODE}
									{$contactdata.zipcode}
								{/if}
								{if $contactdata.city && $template.settings.SETTINGS_SHOW_CONTACT_CITY}
									{$contactdata.city}
								{/if}
							</span></li>
						{/if}
						{if $contactdata.country && $template.settings.SETTINGS_SHOW_CONTACT_COUNTRY}
							<li><span>{$contactdata.country}</span></li>
						{/if}
						{if $contactdata.vatnumber && $template.settings.SETTINGS_SHOW_CONTACT_VAT_NUMBER}
							<li><span>{$text.VAT_NR}: {$contactdata.vatnumber}</span></li>
						{/if}
						{if $contactdata.bankinfo && $template.settings.SETTINGS_SHOW_CONTACT_BANK_NUMBER}
							<li><span>{$text.BANK_DETAILS}: {$contactdata.bankinfo}</span></li>
						{/if}
					</ul>

					<ul class="list-unstyled">
						{if $contactdata.phone && $template.settings.SETTINGS_SHOW_CONTACT_PHONE}
							<li><span>{$text.TELEPHONE}: {$contactdata.phone}</span></li>
						{/if}
						{if $contactdata.mobilephone && $template.settings.SETTINGS_SHOW_CONTACT_MOBILE}
							<li><span>{$text.MOBILE}: {$contactdata.mobilephone}</span></li>
						{/if}
						{if $contactdata.fax && $template.settings.SETTINGS_SHOW_CONTACT_FAX}
							<li><span>{$text.FAX}: {$contactdata.fax}</span></li>
						{/if}
						{if $contactdata.email && $template.settings.SETTINGS_SHOW_CONTACT_EMAIL}
							<li>
								<span class="contact-text">{$text.MAIL}</span><span class="contact-colon">:</span>
								{if $settings.spam_email_block}
									{$email = $contactdata.email|replace:'@':'---'}
									{placeholdImage assign=placeholder text=$email color=$template.settings.FONT_COLOR_PRIMARY atreplace='---' transparent=true size=10}
									<a href="/obfuscated/" onclick="var m = '{$email}'; this.href = 'mailto:' + m.replace('---', '@'); return true;">
										<span>
										{img style="margin:0; vertical-align: middle;" alt="" src="{$placeholder->getRelativeFile()}"}
									</span>
									</a>
								{else}
									<a href="mailto:{$contactdata.email}">{$contactdata.email}</a>
								{/if}
							</li>
						{/if}
					</ul>

					{if $template.settings.SETTINGS_SHOW_SITEMAP}
						<a href="/{$text.SITEMAP_LINK}/">{$text.SITEMAP_HEADLINE}</a>
					{/if}
				</div>
			</div>
			<div class="col-s-4 col-m-6 col-l-3 col-xl-6 pull-right">

				{$socialFooter = !empty($settings.social_instagram_pageurl) ||
					$settings.social_facebook and !empty($settings.social_plugin_likebox_pageurl) ||
					$settings.social_twitter  and !empty($settings.social_twitter_pageurl) ||
					$settings.social_youtube  and !empty($settings.social_youtube_pageurl) ||
					$settings.social_linkedin and !empty($settings.social_linkedin_pageurl)
				}

				{if $access.social and $socialFooter}
				<div class="footer-social">
					<p class="h4">{$text.SOCIAL_MEDIA}</p>

					{if $settings.social_plugin_likebox_pageurl and $settings.social_facebook}
						<a href="{$settings.social_plugin_likebox_pageurl}" title="{$text.SOCIAL_BOX_FACEBOOK}" target="_blank" rel="noopener" class="fa-stack fa-lg social-link social-facebook">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
						</a>
					{/if}
					{if $settings.social_twitter_pageurl and $settings.social_twitter}
						<a href="{$settings.social_twitter_pageurl}" title="{$text.SOCIAL_BOX_TWITTER}" target="_blank" rel="noopener" class="fa-stack fa-lg social-link social-twitter">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
						</a>
					{/if}
					{if $settings.social_youtube_pageurl and $settings.social_youtube}
						<a href="{$settings.social_youtube_pageurl}" title="{$text.SOCIAL_BOX_YOUTUBE}" target="_blank" rel="noopener" class="fa-stack fa-lg social-link social-youtube">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i>
						</a>
					{/if}
					{if $settings.social_linkedin_pageurl and $settings.social_linkedin}
						<a href="{$settings.social_linkedin_pageurl}" title="{$text.SOCIAL_BOX_LINKEDIN}" target="_blank" rel="noopener" class="fa-stack fa-lg social-link social-linkedin">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-linkedin-square fa-stack-1x fa-inverse"></i>
						</a>
					{/if}
					{if $settings.social_instagram_pageurl}
						<a href="{$settings.social_instagram_pageurl}" title="{$text.SOCIAL_BOX_INSTAGRAM}" target="_blank" rel="noopener" class="fa-stack fa-lg social-link social-instagram">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
						</a>
					{/if}
				</div>
				{/if}
			</div>
		</div>
		{if $general.isShop}
			<div class="row">
				<div class="col-s-4 col-m-12 col-l-12 col-xl-24 text-center">
					<hr>
					{if $general.isShop && $template.settings.SETTINGS_SHOW_CART_ICONS_FOOTER}
						<div class="text-center footer-paymenticons">
							{if $template.settings.SETTINGS_SHOW_BIG_CART_ICONS_FOOTER}
								{$imageWidth  = 54}
								{$imageHeight = 30}
								{paymentIcons assign=icons iconSize=large}
							{else}
								{$imageWidth  = 34}
								{$imageHeight = 24}
								{paymentIcons assign=icons}
							{/if}
							{if $icons->getActualSize() gt 0}
								{include file="modules/widgets/image/placeholder-aspect.tpl" 
									width=$imageWidth 
									height=$imageHeight
									selector=":not(.ielt9) .footer-paymenticons"}
								<ul class="payment-icons list-unstyled">
									{foreach $icons->getData() as $icon}
										<li class="payments-icon payments-icon-{$icon@index} is-inline-block placeholder-wrapper">
											<span class="placeholder"></span>
											{img alt="{$icon->Title}" title="{$icon->Title}" src="{$template.cdn}{$icon->RelativeFile}"}
										</li>
									{/foreach}
								</ul>
							{/if}
						</div>
					{/if}
				</div>
			</div>
		{/if}
	</div>
</footer>
