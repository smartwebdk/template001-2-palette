{includeScript file='partials/body.js.tpl'}

<header class="site-header trunk--header trunk--slide trunk--content slideRight">
    <div class="container with-xlarge">
        <div class="row">
            <div class="col-s-2 col-m-6 col-l-6 col-xl-12">
                <div style="display:table;width:100%;">
                {include file='modules/widgets/logo/logo.tpl'}
                </div>
            </div>
            <div class="trunk--navigation col-s-2 col-m-6 col-l-6 col-xl-12 is-hidden-print">

                <div class="pull-right">
                    <button id="navToggle" type="button" title="{$text.WHERE_AM_I_NAVIGATION}" class="button-nav"><i class="fa fa-bars fa-fw"></i></button>
                </div>

                {if $general.isShop && $template.settings.SETTINGS_SHOW_CART and $page.type != 'cart' && $page.type != 'checkout'}
                    {collection controller=cart assign=cart}
                    {include file='modules/widgets/cart/cart.tpl' cart=$cart}
                {/if}

            </div>
        </div>
    </div>
</header>